<?php

class ServiceFunctions
{
    public $db;

    public function __construct()
    {
        $dbc['host'] = "localhost";
        $dbc['user'] = "root";
        $dbc['pass'] = "";
        $dbc['name'] = "iai";
        $dbc['encode'] = "SET NAMES utf8";

        $dbc['dns'] = 'mysql:host=' . $dbc['host'] . ';dbname=' . $dbc['name'];
        $dbc['options'] = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        );
        try {
            $this->db = new PDO($dbc['dns'], $dbc['user'], $dbc['pass'], $dbc['options']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Wystąpił błąd przy łączeniu z bazą danych.');
        }
        unset($dbc);

    }

    public function getAllProducts()
    {
        $result = $this->db->prepare("SELECT id, name, price FROM products");
        $result->execute();

        while ($row = $result->fetch()) {
            $array['id'][] = $row['id'];
			$array['name'][] = $row['name'];
			$array['price'][] = $row['price'];
        }
        return json_encode($array);

    }

	    public function getAllClients()
    {
        $result = $this->db->prepare("SELECT id, name, email FROM clients");
        $result->execute();

        while ($row = $result->fetch()) {
            $array['id'][] = $row['id'];
			$array['name'][] = $row['name'];
			$array['email'][] = $row['email'];
        }
        return json_encode($array);

    }
}