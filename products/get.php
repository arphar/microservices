<?php
header("Content-type: json");

require_once ('class/soap.class.php');

$options = array(
    'uri' => 'http://localhost',
    'location' => 'http://localhost/iai/soap_server.php',
    'trace' => 1
);

$client = new SoapClient(NULL, $options);

//echo $client->getInitials('Cezary', 'Dziekonski');

if($_GET['type'] == 'products')
echo($client->getAllProducts());

if($_GET['type'] == 'clients')
echo($client->getAllClients());