<?php
require_once 'model/Model.php';

class ProductModel extends Model {
	protected $query;
	
    public function query($query) {
		$this->query = mysqli_query($this->pdo, $query);
	}

    public function getAffectedRows() {
		return mysqli_affected_rows($this->pdo);
	}

    public function getRow() {
		return mysqli_fetch_row($this->query);
	}

    public function getAllRows() {
		$results = array();
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        return $results;
	}
}
