<?php
require_once 'model/Model.php';

class UserModel extends Model {
	protected $query;
	
    
    public function query($query) {
		$this->query = mysqli_query($this->pdo, $query);
	}

    public function getAffectedRows() {
		return mysqli_affected_rows($this->pdo);
	}

    public function getRow() {
		return mysqli_fetch_row($this->query);
	}

    public function getAllRows($arguments) {
		$results = array();
        
        while (($row = $this->getRow())) {
            $i = 0;
            foreach ($arguments as $argument)
            {
                $arrayTest[$argument] = $row[$i];
                $i++;
            }
            $results[] = $arrayTest;
        }
        return $results;
	}

    public function getAllUsers ()
    {
        $arguments = ['name', 'surname', 'email'];
        $result = $this->query("SELECT name, surname, email FROM clients WHERE email NOT LIKE \"\"");
        
        return $this->getAllRows($arguments);
    }
}
