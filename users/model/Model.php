<?php

abstract class Model {
    protected $pdo;
    
    public function  __construct() 
    {
        $this->setSQLConnection();
    }
    
    private function setSQLConnection()
    {
        try {
            require 'config/sql.php';
            /*$this->pdo=new PDO(
                    'mysql:host=' .  SQL_HOST . ';dbname=' . SQL_DBASE, 
                    SQL_USER, 
                    SQL_PASSWORD
            );
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			*/
			$this->pdo = mysqli_connect(SQL_HOST, SQL_USER, SQL_PASSWORD, SQL_DBASE);
        }
        catch(DBException $e) {
            echo 'The connect can not create: ' . $e->getMessage();
            exit;
        }
    }
    
    public function loadModel($name, $path = 'model/') 
    {
        $modelName = $name . 'Model';
        $modelPath = $path . $name . '.php';
        
        try {
            if (!file_exists($modelPath)) {
                throw new Exception('Can not open model ' . $modelName);
            } else {
                require $modelPath;
                $model = new $modelName();
            }
        } catch (Exception $e) {
            $e->getMessage();
            exit;
        }
        
        return $model;
    }
    
    
}
