<?php

require_once 'controller/Controller.php';

class UserController extends Controller {
    public function form()
    {
        $model = $this->loadModel('User');
        $array = $model->getAllUsers();
        $jsonArray = json_encode($array);
        print_r($jsonArray);
    }
}
