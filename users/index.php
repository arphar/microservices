<?php

$controllerPath = (isset($_GET['controller'])) ? ucfirst($_GET['controller']) : 'User';
$controllerName = (isset($_GET['controller'])) ? ucfirst($_GET['controller']) . 'Controller' : 'UserController';
$action = (isset($_GET['action'])) ? $_GET['action'] : 'form';

$choice = (isset($_GET['choice'])) ? $_GET['choice'] : '';

$arguments[] = (isset($_GET['name'])) ? $_GET['name'] : '';
$arguments[] = (isset($_GET['price'])) ? $_GET['price'] : '';
$arguments[] = (isset($_GET['id'])) ? $_GET['id'] : '';

$controllerFile = 'controller/' . $controllerPath . '.php';
if (!file_exists($controllerFile)) {
    echo 'Unexpected controller name: ' . $controllerName;
    exit;
}

require_once $controllerFile;

if($choice != '') {
	$controller = new $controllerName();
	$controller->$choice($arguments);
}
else {
	$controller = new $controllerName();
	$controller->$action();
}

