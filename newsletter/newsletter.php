<?php

$usersLink = 'http://10.26.85.113/day3/index.php';
$productsLink = 'http://10.26.85.114/iai/get.php?type=products';

/*Funkcja pobierajaca dane w formacie JSON z API udostepnionego pod adresami $usersLink
oraz $productsLink i zwracajaca je po zdekodowaniu do tablicy.*/

function getData ($action, $link){

	if($action == 'getUsers'){
		$ch = curl_init($link);
	} elseif($action == 'getProducts') {
		$ch = curl_init($link);
	} else {
		echo "Nieprawidlowa akcja";
	}


//	$token = '5cf741c362c9431673dccbc578baa705';

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result = curl_exec($ch);

	$input = json_decode($result, true);
	return $input;

}

/*Funkcja przygotowujaca tresc wiadomosci;*/
function prepareMessage($firstName, $lastName, $productsData){
	

		$messageFirstPart = '
		<html>
		<head>
		  <title>Super oferta dla'.$firstName.' '.$lastName.'</title>
		</head>
		<body>
		  <p>Najlepsze produkty w tym miesiacu!</p>
		  	<table>
		    <tr>
		      <th>Produkt</th><th>Cena</th>
		    </tr>';

		$table = '';


		//TODO: dostosowac do danych odbieranych z API
		foreach ($productsData as $key) {
			$table = $table.'<tr><td>'.$key['name'].'</td><td>'.$key['price'].'</td></tr>';
		}

		$messageLastPart = '</table></body></html>';

		return $messageFirstPart.' '.$table.' '.$messageLastPart;
}

/*
Funkcja wysylajaca wiadomosc o tresci $message pod wskazany $email o tytule $subject 
*/
function sendMail($email, $subject, $message){
	$headers = 'From: phpcamp@phpcamp.pl' . "\r\n" .
    'Reply-To: phpcamp@phpcamp.pl' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

	//mail($email, $subject, $message, $headers);
}


$subject = 'Super okazyjna oferta!';

$userData = getData('getUsers', $usersLink);
$productsData = getData('getProducts', $productsLink);
//var_dump($productsData);


foreach ($userData as $key) {
	$message = prepareMessage($key['name'],$key['surname'], $productsData);
	sendMail($key['email'], $subject, $message);
}


die;
?>